<?php

namespace App\Controller;

use App\Entity\Barbecue;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $entityManager;

    private $barbecueRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->barbecueRepository = $entityManager->getRepository(Barbecue::class);
    }

    public function index()
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/search", name="search") 
     * */
    public function search(Request $request): Response
    {
        $zip = $request->get('zip');
        $country = $request->get('country');
        $address = $request->get('address');

        $barbecues = $this->barbecueRepository->findByZipAndCountry($zip, $country);
        return $this->render('results_page.html.twig', [
            'barbecues' => $barbecues,
            'zip' => $zip,
            'country' => $country,
            'address' => $address,
        ]);
    }

    /**
     * @Route("/post", name="post") 
     * */
    public function post(Request $request): Response
    {
        if ("POST" === $request->getMethod())
        {
            $image = $request->files->get('imageFile');
            $owner = $this->getUser();
            $barbecue = new Barbecue(
                $request->get('model'),
                $request->get('properties'),
                $request->get('description'),
                $request->get('latitude'),
                $request->get('longitude'),
                $request->get('zip'),
                $request->get('country'),
                $owner
            );
            $barbecue->setImageName(md5(uniqid()) . '.' . $image->guessExtension());
            $barbecue->setImageSize($image->getClientSize());
            $barbecue->setImageFile($image);
        
            $this->entityManager->persist($barbecue);
            $this->entityManager->flush();
            $this->addFlash("success", 'Barbacoa agregada exitosamente!');
        }    
        return $this->render('post_page.html.twig');
    }

    /**
     * @Route("/rent", name="rent", methods={"POST"}) 
     * */
    public function rent(Request $request): Response
    {
        $renter = $this->getUser();
        $bbqId = $request->get('barbecue_id');
        $barbecue = $this->barbecueRepository->findOneById($bbqId);

        $barbecue->setRenter($renter);
        $this->entityManager->persist($barbecue);
        $this->entityManager->flush();

        return $this->render('rent_success.html.twig');

    }

    /**
     * @Route("/history", name="history", methods={"GET"}) 
     * */
    public function rentedBarbecues(Request $request): Response
    {
        $renterId = $this->getUser()->getId();
        $barbecues = $this->barbecueRepository->findByRenterId($renterId);
        return $this->render('rented_barbecues.html.twig', ['barbecues' => $barbecues]);
    }

    /**
     * @Route("/owned", name="owned", methods={"GET"}) 
     * */
    public function ownedBarbecues(Request $request): Response
    {
        $ownerId = $this->getUser()->getId();
        $barbecues = $this->barbecueRepository->findByOwnerId($ownerId);
        return $this->render('owned_barbecues.html.twig', ['barbecues' => $barbecues]);
    }

}