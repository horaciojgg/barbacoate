<?php

namespace App\Repository;

use App\Entity\Barbecue;
use Doctrine\ORM\EntityRepository;

class BarbecueRepository extends EntityRepository
{
    public function findByOwnerId(int $ownerId): ?Array
    {
        return $this->findBy(['owner' => $ownerId]);
    }

    public function findByRenterId(int $renterId): ?Array
    {
        return $this->findBy(['renter' => $renterId]);
    }

    public function findByZipAndCountry(string $zip, string $country): ?Array
    {
        return $this->findBy([
            'zip' => $zip,
            'country' => $country
        ]);
    }

    public function findOneById(int $id): ?Barbecue
    {
        return $this->findOneBy(['id' => $id]);
    }
}
