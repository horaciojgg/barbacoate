<?php

namespace App\Entity;

use App\Entity\Barbecue;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="User")
 */
class User Extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    
    /**
     * @var mixed[]
     * @ORM\OneToMany(targetEntity="App\Entity\Barbecue", mappedBy="owner")
     */
    private $ownedBarbecues;


    /**
     * @var mixed[]
     * @ORM\OneToMany(targetEntity="App\Entity\Barbecue", mappedBy="renter")
     */
    private $rentedBarbecues;


    public function __construct()
    {
        parent::__construct();
    }

    public function getId(): int
    {
        return $this->id;
    }
}