<?php

namespace App\Entity;

use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;


/**
 * @ORM\Entity(repositoryClass="App\Repository\BarbecueRepository")
 * @ORM\Table(name="Barbecue")
 * @Vich\Uploadable
 */
class Barbecue
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var String
     * @ORM\Column(type="string", name="model")
     */
    private $model;

    /**
     * @var String
     * @ORM\Column(type="string", name="properties")
     */
    private $properties;

    /**
     * @var String
     * @ORM\Column(type="string", name="description")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ownedBarbecues")
     * @var ArrayCollection
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="rentedBarbecues")
     */
    private $renter = null;

    /**
     * @var String
     * @ORM\Column(type="string", name="zip")
     */
    private $zip;

    /**
     * @var String
     * @ORM\Column(type="string", name="country")
     */
    private $country;

    /**
     * @var String
     * @ORM\Column(type="string", name="latitude")
     */
    private $latitude;

    /**
     * @var String
     * @ORM\Column(type="string", name="longitude")
     */
    private $longitude;

    /**
     * @Vich\UploadableField(mapping="bbq_image", fileNameProperty="imageName", size="imageSize")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct(
        string $model,
        string $properties,
        string $description,
        string $latitude,
        string $longitude,
        string $zip,
        string $country,
        User $owner,
        User $renter = null
    )
    {
        $this->model = $model;
        $this->properties = $properties;
        $this->description = $description;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->zip = $zip;
        $this->country = $country;
        $this->owner = $owner;
        $this->renter = $renter;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model)
    {
        $this->model = $model;
    }

    public function getProperties(): ?string
    {
        return $this->properties;
    }

    public function setProperties(string $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function getRenter(): ?User
    {
        return $this->renter;
    }

    public function setRenter(User $renter): self
    {
        $this->renter = $renter;
        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;
        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @param File|UploadedFile $image
     */
    public function setImageFile(?File $image = null)
    {
        $this->imageFile = $image;

        if (null !== $image) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }
    
    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
}
